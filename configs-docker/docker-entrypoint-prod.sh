#!/bin/sh

cd /var/www/html

# Finally add owner, because maybe any log file was generated during build
chown -R 82:82 /var/www/html/storage

php-fpm -g /var/run/php-fpm.pid

php artisan storage:link